##D 语言入门书籍《Programming in D》中文翻译项目。##

### 当前翻译完成的页面 ###

- [The Hello World Program](helloworld.html) By Lucifer
- [writeln and write](writeln.html) By Lucifer
- [Compiler](compiler.html) By Lucifer
- [Fundamental Types](types.html) By Lucifer
- [Assignment and Order of Evaluation](assignment.html) By Lucifer
- [Variables](variables.html) By Lucifer
- [Standard Input and Output Streams](io.html) By Lucifer
- [Classes](class.html) By 大处着手小处着眼
- [Associative Arrays](aa.html) By 大处着手小处着眼
- [Strings](strings.html) By 大处着手小处着眼
- [scope](scope.html) By 大处着手小处着眼
- [auto and typeof](auto.html) By 小马哥[̲̅V̲̅I̲̅P̲̅]

### 当前正在翻译中的页面 ###

- [Arrays](arrays.html) By 大灰熊
- [Reading from the Standard Input](input.html) By Lucifer
- [Conditional Compilation](condcomp.html) By 小马哥[̲̅V̲̅I̲̅P̲̅]
- [is Expression](isexpr.html) By 小马哥[̲̅V̲̅I̲̅P̲̅]
- [Object](object.html) By 大处着手小处着眼
- [Tuples](tuples.html) By 大处着手小处着眼
- [Immutability](immutability.html) By 大处着手小处着眼
- [Ranges](ranges.html) By 大处着手小处着眼
- [Mixins](mixin.html) By 大处着手小处着眼
- [Function Pointers, Delegates, and Lambdas](lambda.html) By 大处着手小处着眼
- [Memory Management](memory.html) By 大处着手小处着眼
- [Functions](functions.html) By 大处着手小处着眼
- [Function Parameters](functionparameters.html) By 大处着手小处着眼
- [Function Overloading](functionoverloading.html) By 大处着手小处着眼
- [More Functions](functionsmore.html) By 大处着手小处着眼

### 当前还未翻译的页面 ###

- [Acknowledgments](acknowledgments.html)
- [Introduction](intro.html)
- [Practice of Programming](programming.html)
- [Logical Expressions](logicalexpressions.html)
- [if Statement](if.html)
- [while Loop](while.html)
- [Integers and Arithmetic Operations](arithmetic.html)
- [Floating Point Types](floatingpoint.html)
- [Characters](characters.html)
- [Slices and Other Array Features](slices.html)
- [Redirecting Standard Input and Output Streams](streamredirect.html)
- [Files](files.html)
- [Name Space](namespace.html)
- [for Loop](for.html)
- [Ternary Operator ?:](ternary.html)
- [Literals](literals.html)
- [Formatted Output](formattedoutput.html)
- [Formatted Input](formattedinput.html)
- [do-while Loop](dowhile.html)
- [foreach Loop](foreach.html)
- [switch and case](switchcase.html)
- [enum](enum.html)
- [Lazy Operators](lazy.html)
- [Program Environment](main.html)
- [Exceptions](exceptions.html)
- [assert and enforce](assert.html)
- [Unit Testing](unittesting.html)
- [Contract Programming](contracts.html)
- [Lifetimes and Fundamental Operations](lifetimes.html)
- [Value Types and Reference Types](valuevsreference.html)
- [The null Value and the is Operator](nullis.html)
- [Type Conversions](cast.html)
- [Structs](struct.html)
- [Variable Number of Parameters](parameterflexibility.html)
- [Member Functions](memberfunctions.html)
- [const ref Parameters and const Member Functions](constmemberfunctions.html)
- [Constructor and Other Special Functions](specialfunctions.html)
- [Operator Overloading](operatoroverloading.html)
- [Inheritance](inheritance.html)
- [Interfaces](interface.html)
- [destroy and scoped](destroy.html)
- [Modules and Libraries](modules.html)
- [Encapsulation and Access Rights](encapsulation.html)
- [Universal Function Call Syntax (UFCS)](ufcs.html)
- [Properties](property.html)
- [Contract Programming for Structs and Classes](invariant.html)
- [Templates](templates.html)
- [alias](alias.html)
- [alias this](aliasthis.html)
- [Pointers](pointers.html)
- [Bit Operations](bitoperations.html)
- [foreach with Structs and Classes](foreachopapply.html)
- [Unions](union.html)
- [Labels and goto](goto.html)
- [More Templates](templatesmore.html)
- [More Ranges](rangesmore.html)
- [Parallelism](parallelism.html)
- [Message Passing Concurrency](concurrency.html)
- [Data Sharing Concurrency](concurrency_shared.html)
- [User Defined Attributes (UDA)](uda.html)


### 原文地址在 [http://ddili.org/ders/d.en/index.html](http://ddili.org/ders/d.en/index.html) ###